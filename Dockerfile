FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive


RUN apt-get update && apt-get install -y -qq openconnect squid3
RUN apt-get install -y -qq curl wget libnet-nslookup-perl iputils-ping net-tools dnsutils

ADD squid.conf /etc/squid/squid.conf
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT /entrypoint.sh

# RUN echo "America/Argentina/Buenos_Aires" > /etc/timezone && rm /etc/localtime && dpkg-reconfigure -f noninteractive tzdata


# OPENCONNECT_URL=https://EXAMPLE_URL_COM
# OPENCONNECT_USER=EXAMPLE_USER
# OPENCONNECT_PROTOCOL=pulse

# --protocol=anyconnect       Compatible with Cisco AnyConnect SSL VPN, as well as ocserv (default)
# --protocol=nc               Compatible with Juniper Network Connect
# --protocol=gp               Compatible with Palo Alto Networks (PAN) GlobalProtect SSL VPN
# --protocol=pulse            Compatible with Pulse Connect Secure SSL VPN


