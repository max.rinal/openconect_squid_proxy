IMAGE_NAME=openconnect_proxy
CONTAINER_NAME=c_proxy

build:
	docker build -t $(IMAGE_NAME) .

run:
	 docker run  --env-file .env --rm --privileged -p 3128:3128 --name c_proxy -it openconnect_proxy


exec:
	 docker exec -it $(CONTAINER_NAME) bash
