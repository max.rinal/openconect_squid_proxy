# Usage 

Using docker it will start an image, connect to vpn and run a squid proxy on port 3128

### Build Image

```
make build
```

### Create env_file with vars


```
cat <<'EOF' >> /tmp/env_file
OPENCONNECT_URL=https://EXAMPLE_URL_COM
OPENCONNECT_USER=EXAMPLE_USER
OPENCONNECT_PROTOCOL=valid_protocol
EOF
```


### run

```
make run
```



#### Available protocols

```
--protocol=anyconnect       Compatible with Cisco AnyConnect SSL VPN, as well as ocserv (default)
--protocol=nc               Compatible with Juniper Network Connect
--protocol=gp               Compatible with Palo Alto Networks (PAN) GlobalProtect SSL VPN
--protocol=pulse            Compatible with Pulse Connect Secure SSL VPN
```


