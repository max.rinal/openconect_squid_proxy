#!/bin/bash

sed "s/port: .*$/port: $PROXY_PORT/" -i /etc/squid/squid.conf
# cat /etc/squid/squid.conf


openconnect --background --protocol=${OPENCONNECT_PROTOCOL} -u "$OPENCONNECT_USER" --no-dtls $OPENCONNECT_URL --disable-ipv6

service squid start


sleep infinity
